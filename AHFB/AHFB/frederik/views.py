"""
Definition of views.
"""

from django.shortcuts import render
from django.http import HttpRequest
from django.template import RequestContext
from datetime import datetime

def index(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,

        'frederik/index.html',
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )

def game(request):
    """Renders the game page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,

        'frederik/game.html',
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )