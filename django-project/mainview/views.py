from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import JsonResponse, HttpRequest, HttpResponse


def login(request):
    pass

def combat(request):
    pass

def main_page(request):
    if request.method == "GET":
        return render(request, 'createuser/index.html')
    # Some action was submitted
    # Reroute it to the relevant view
    elif "action" in request.POST:
        return internal_routes[request.POST["action"]](request)

def create_user(request):
    # Create user case
    if "user" in request.POST and "password" in request.POST and "confirm" in request.POST:
        if request.POST["password"] == request.POST["confirm"]:
            user = User.objects.create_user(request.POST["user"], password=request.POST["password"])
            user.save()
            return render(request, 'combat/index.html')
    # Error case
    return HttpResponse("", status=400)

# Create your views here.

internal_routes = {
    "createuser": create_user,
    "login": login,
    "combat": combat
}
