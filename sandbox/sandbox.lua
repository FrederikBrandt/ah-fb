-- run code under environment [Lua 5.2]
function (untrusted_code, game_env)
    local env = {
        ipairs=ipairs,
        pairs=pairs,
        pcall=pcall,
        select=select,
        unpack=unpack,
        format=string.format,
        abs=math.abs,
        ceil=math.ceil,
        exp=math.exp,
        floor=math.floor,
        fmod=math.fmod,
        log=math.log,
        max=math.max,
        min=math.min,
        modf=math.modf,
        pow=math.pow,
        random=math.random,
        sqrt=math.sqrt,
        game=game_env,
    }
    local untrusted_function, message = load(untrusted_code, nil, 't', env)
    if not untrusted_function then return "=== Syntax Error ===", message end
      return untrusted_function()
end
