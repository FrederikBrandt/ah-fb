import lupa
from lupa import LuaRuntime

class ScriptEnd(Exception):
    pass

class Game():
    def __init__(self):
        pass
    def game_action (self, msg):
        print ("Doing %s" % msg)
        raise ScriptEnd()

def getable(obj, attr_name):
    if not attr_name.startswith('_'):
        return getattr(obj, attr_name)
    raise AttributeError('Attribute not found')

def setable(obj, attr_name):
    raise AttributeError('Game object is read only')

with open("sandbox.lua") as f:
    sandbox_code = f.read()
    lua_sandbox = LuaRuntime(unpack_returned_tuples=True, attribute_handlers=(getable, setable), )
    safe_lua_run = lua_sandbox.eval(sandbox_code)

for test_file in ['test.lua', 'test_empty.lua', 'test_syntax.lua', 'test_failure.lua']:
    with open(test_file) as f2:
        game_script = f2.read()
        game_inst = Game()
        try:
            ret_val = safe_lua_run(game_script, Game())
        except ScriptEnd:
            ret_val = "=== Execution Success ==="
        except Exception as e:
            ret_val = ("=== Execution Error ===", e)
        if not ret_val:
            ret_val = "=== Warning: Ended without action ==="
        print (ret_val)


