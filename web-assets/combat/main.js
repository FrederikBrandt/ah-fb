var dragged;

function on_portrait_click (ev) {
  var p_list = document.querySelectorAll(".portrait");
  for (var i = 0; i < p_list.length; i++){
    p_list[i].style.border = "none"
  }
  ev.target.style.border = "3px solid green"
}

function on_portrait_drop(ev) {
    //alert("You dropped") Sometimes fires when dropped. But seems to be only when it is dropped as a result of the script, not when you release the drag.
}

function on_place_dragover(ev) {
  ev.preventDefault();
}

function on_portrait_drag (ev) {
  dragged = ev.target;
  ev.dataTransfer.setData("tutu", 1)
}

function on_place_dropover (ev) {
    ev.preventDefault();
  if (ev.currentTarget.querySelectorAll("div.portrait").length < 5){
    ev.currentTarget.appendChild(dragged)
  }
}

var p_list = document.querySelectorAll("div.portrait");
for (var i = 0; i < p_list.length; i++){
  p_list[i].onclick = on_portrait_click;
  p_list[i].draggable = true;
  p_list[i].ondrop = on_portrait_drop;
  p_list[i].ondragstart = on_portrait_drag;
  p_list[i].ondragover = on_place_dragover
}

var place_list = document.querySelectorAll("div.front, div.back")
for (var i = 0; i < place_list.length; i++){
  place_list[i].ondrop = on_place_dropover
  place_list[i].ondragover = on_place_dragover
};
